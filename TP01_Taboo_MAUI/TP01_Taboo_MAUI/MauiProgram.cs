﻿using Microsoft.Extensions.Logging;
using CommunityToolkit.Maui;
using TP01_Taboo_MAUI.Views;
using TP01_Taboo_MAUI.ViewModel;


namespace TP01_Taboo_MAUI;

public static class MauiProgram
{
    public static MauiApp CreateMauiApp()
    {
        var builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .UseMauiCommunityToolkit()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
            });

        builder.Services.AddSingleton<MainPage>();
        builder.Services.AddSingleton<HomePage>();
        builder.Services.AddSingleton<HomeViewModel>();
        builder.Services.AddTransient<PlayPage>();
        builder.Services.AddTransient<PlayViewModel>();
        builder.Services.AddTransient<ConfirmPage>();
        builder.Services.AddTransient<ConfirmViewModel>();
        builder.Services.AddTransient<EndPage>();
        builder.Services.AddTransient<EndViewModel>();


#if DEBUG
        builder.Logging.AddDebug();
#endif

        return builder.Build();
    }
}

