﻿using System.Timers;
using TP01_Taboo_MAUI.Interfaces;
using Timer = System.Timers.Timer;

namespace TP01_Taboo_MAUI.Models
{
    public class GameTimer
    {
        /******************************************************************************************
        * ATTRIBUTES
        *****************************************************************************************/

        public static int SECOND = 1000;
        public static int HUNDRED_MILLIS = 100;      // timer screen update

        private static IOnGameTimerEvent listener;   // listener for timer update

        private Timer countDownTimer;

        DateTime _startTime;
        CancellationTokenSource _cancellationTokenSource;

        private bool paused;        // is the countdown timer paused or not
        //private long countDownLeft; 
        private long countDownTime; // remaining time left when paused
        private long countDownTick; // interval for updates

        /******************************************************************************************
         * SINGLETON PATTERN
         **************************************************************************************** */

        private static GameTimer instance;  // unique instance

        private GameTimer() { paused = false; } // constructor

        /**
         * Get the unique instance of the GameTimer class
         * @param listener used for timer updates
         * @return unique instance of GameTimer class
         */
        public static GameTimer GetInstance(IOnGameTimerEvent listener)
        {
            instance ??= new GameTimer();
            GameTimer.listener = listener;
            return instance;
        }

        /* *****************************************************************************************
         * PUBLIC METHODS
         **************************************************************************************** */

        /**
         * Start the countdown timer
         * @param countDownTime total time
         * @param countDownTick interval for updates
         */
        public void StartTimer(long countDownTime, long countDownTick)
        {
            this.countDownTick = countDownTick;
            Start(countDownTime, countDownTick);
        }

        /**
         * Resume the countdown timer
         */
        public void ResumeTimer()
        {
            if (paused)
            {
                Start(countDownTime, countDownTick);
            }
        }

        /**
         * Pause the countdown timer
         */
        public void PauseTimer()
        {
            if (countDownTimer != null)
            {
                paused = true;
                countDownTimer.Stop();
            }
        }

        /**
         * Stop and destroy the countdown timer
         */
        public void StopTimer()
        {
            if (countDownTimer != null)
            {
                countDownTimer.Stop();
                countDownTimer.Dispose();
                countDownTimer = null;
            }
        }

        /**
         * Stop and destroy the countdown timer
         */
        public void SaveState()
        {
            paused = true;
        }

        /**
         * Subtract a certain amount of time
         * @param millis number of time to subtract
         */
        public void SubtractMillis(long millis)
        {
            countDownTimer.Stop();
            countDownTime -= millis;

            if (countDownTime <= 0)
            {
                listener.onFinish();
            }
            else
            {
                Start(countDownTime, countDownTick);
            }
        }

        /* *****************************************************************************************
         * PRIVATE METHODS
         **************************************************************************************** */

        /**
         * Internal method to handle the start of the countdown
         * @param time total time
         * @param tick interval for updates
         */
        private void Start(long time, long tick)
        {
            paused = false;

            countDownTime = time;

            countDownTimer = new Timer();
            countDownTimer.Interval = tick;
            countDownTimer.Elapsed += OnTimedEvent;
            countDownTimer.Start();
        }

        //*****************************************************************

        private void OnTimedEvent(System.Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}, Countdown time: {1}",
                              e.SignalTime, countDownTime);

            if (countDownTime > 0)
            {
                countDownTime -= 1000;

                listener.onTick(countDownTime);
            }
            else
            {
                countDownTimer.Stop();
                countDownTimer.Dispose();
                listener.onFinish();
            }

        }
    }
}


