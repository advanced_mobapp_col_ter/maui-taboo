﻿using System;
namespace TP01_Taboo_MAUI.Models
{
	public class TeamScore
	{

		public ImageSource Image { get; set; }
		public string TeamName { get; set; }
		public string Score { get; set; }

		public TeamScore(ImageSource im, string tName, string score)
		{
			Image = im;
			TeamName = tName;
			Score = score;
		}

	}
}

