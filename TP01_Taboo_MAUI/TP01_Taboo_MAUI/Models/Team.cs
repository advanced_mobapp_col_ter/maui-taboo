﻿using System;
namespace TP01_Taboo_MAUI.Models
{
	public class Team
	{
        public String name { get; set; }       // team name
        public int score { get; set; }         // team score

        /**
         * Constructor creating a team
         * @param name team name
         */
        public Team(String name)
        {
            this.name = name;
        }

        /**
         * Increment the team score
         */
        public void IncrementScore()
        {
            this.score++;
        }
    }
}

