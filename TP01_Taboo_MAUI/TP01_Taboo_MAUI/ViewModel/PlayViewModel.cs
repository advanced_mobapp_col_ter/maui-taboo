﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using TP01_Taboo_MAUI.Models;
using TP01_Taboo_MAUI.Views;

namespace TP01_Taboo_MAUI.ViewModel
{

    // set the QueryProperty to get the parameters 
    [QueryProperty(nameof(PlayParams), "PlayParams")]
    public partial class PlayViewModel : ObservableObject, TabooManager.IOnTabooUpdate
    {


        public TabooManager TabooManagerInstance { get; }

        [ObservableProperty]
        PlayParameter playParams;

        // set the observable properties
        [ObservableProperty]
        int remainingCards = 0;

        [ObservableProperty]
        string team = "";

        [ObservableProperty]
        int remainingTime = 0;

        [ObservableProperty]
        double progressTime = 1.0;

        [ObservableProperty]
        string currentWord = "";

        [ObservableProperty]
        string word1 = "";

        [ObservableProperty]
        string word2 = "";

        [ObservableProperty]
        string word3 = "";

        [ObservableProperty]
        string word4 = "";

        double totalTime = 0;

        [ObservableProperty]
        int totalErrors = 0;

        [ObservableProperty]
        int totalSuccess = 0;

        [ObservableProperty]
        int totalPass = 0;



        //------------------------------
        public PlayViewModel()
        {
            TabooManagerInstance = TabooManager.GetInstance(this);
        }

        partial void OnPlayParamsChanged(PlayParameter value)
        {
            totalTime = value.Duration;

            StartTurn();
        }

        //--------------------------------------------
        public async void StartTurn()
        {
            // retrieve parameters from HomeViewModel...
            // and setup TabooManager.
            if (PlayParams.NewGame)
            {
                await TabooManagerInstance.SetupGame(PlayParams.IsHard, PlayParams.Teams, PlayParams.Cards, PlayParams.Duration, PlayParams.Pass, PlayParams.Penalty);
            }

            // start the turn
            TabooManagerInstance.StartTurn();

            // Update UI
            UpdateUI();

        }

        //**********************************************

        private void UpdateUI()
        {
            RemainingCards = TabooManagerInstance.GetNumberOfRemainingCards();
            Team = TabooManagerInstance.GetCurrentTeam();
            CurrentWord = TabooManagerInstance.CurrentCard.Word;
            Word1 = TabooManagerInstance.CurrentCard.Taboos[0];
            Word2 = TabooManagerInstance.CurrentCard.Taboos[1];
            Word3 = TabooManagerInstance.CurrentCard.Taboos[2];
            Word4 = TabooManagerInstance.CurrentCard.Taboos[3];
            TotalErrors = TabooManagerInstance.Failures;
            TotalSuccess = TabooManagerInstance.Successes;
            TotalPass = TabooManagerInstance.AvailablePass;
        }

        //**************************
        // *** 3 Buttons command ***
        //**************************

        [RelayCommand]
        void ClickError()
        {
            TabooManagerInstance.Failure();
            UpdateUI();
        }

        [RelayCommand]
        void ClickPass()
        {
            TabooManagerInstance.Pass();
            UpdateUI();
        }

        [RelayCommand]
        void ClickCorrect()
        {
            TabooManagerInstance.Success();
            UpdateUI();
        }

        //******************************************************
        // Interface implementation
        //******************************************************
        public void OnTabooTick(int remainingTime, int progress)
        {
            RemainingTime = remainingTime;
            ProgressTime = (double)progress / (double)100;
        }

        public void OnTurnFinished()
        {

            ProgressTime = 0;

            PlayParams.Cards = TabooManagerInstance.GetNumberOfRemainingCards();
            PlayParams.Duration = TabooManagerInstance.GetTurnDuration();
            PlayParams.Pass = TabooManagerInstance.GetNumberOfPass();
            PlayParams.Penalty = TabooManagerInstance.ErrorPenalty;

            // Go to the confirm page
            MainThread.InvokeOnMainThreadAsync(NavigateToConfirmPage);


        }

        private async Task NavigateToConfirmPage()
        {
            await Shell.Current.GoToAsync(nameof(ConfirmPage), new Dictionary<string, object>
             {
                {"PlayParams", PlayParams}
             });
        }
    }
}

