﻿
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Maui.Alerts;
using TP01_Taboo_MAUI.Views;
using CommunityToolkit.Maui.Core;
using TP01_Taboo_MAUI.Models;


namespace TP01_Taboo_MAUI.ViewModel
{
    public partial class HomeViewModel : ObservableObject
    {
        // Observable object and properties
        [ObservableProperty]
        string team1;

        [ObservableProperty]
        string team2;

        [ObservableProperty]
        string team3;

        [ObservableProperty]
        string team4;

        [ObservableProperty]
        string cards;

        [ObservableProperty]
        string duration;

        [ObservableProperty]
        string pass;

        [ObservableProperty]
        string penalty;

        [ObservableProperty]
        bool isHardMode;


        //*******************************************

        public HomeViewModel()
        {
            // Do nothing
        }


        // Relay command to start the game...
        [RelayCommand]
        public async void StartGame()
        {
            // Check if the fields are filled
            if (string.IsNullOrEmpty(Team1) || string.IsNullOrEmpty(Team2))
            {
                // Display a toast
                await Toast.Make("Fill the team names", ToastDuration.Long).Show();
                return;
            }

            // Check the remaining fields
            int nbCards = -1;
            int duration = -1;
            int nbPass = -1;
            int nbPenalty = -1;


            if (string.IsNullOrEmpty(Cards))
            {
                await Toast.Make("Fill the number of cards", ToastDuration.Long).Show();
                return;
            }
            else if (!int.TryParse(Cards, out nbCards))
            {
                await Toast.Make("The number of cards must be a number", ToastDuration.Long).Show();
                return;
            }

            if (string.IsNullOrEmpty(Duration))
            {
                await Toast.Make("Fill the duration", ToastDuration.Long).Show();
                return;
            }
            else if (!int.TryParse(Duration, out duration))
            {
                await Toast.Make("The duration must be a number", ToastDuration.Long).Show();
                return;
            }

            if (string.IsNullOrEmpty(Pass))
            {
                await Toast.Make("Fill the number of pass", ToastDuration.Long).Show();
                return;
            }
            else if (!int.TryParse(Pass, out nbPass))
            {
                await Toast.Make("The number of pass must be a number", ToastDuration.Long).Show();
                return;
            }

            if (string.IsNullOrEmpty(Penalty))
            {
                await Toast.Make("Fill the number of penalty", ToastDuration.Long).Show();
                return;
            }
            else if (!int.TryParse(Penalty, out nbPenalty))
            {
                await Toast.Make("The number of penalty must be a number", ToastDuration.Long).Show();
                return;
            }


            // Add and check if team3 and team4 are filled
            List<string> teams = new List<string>();
            teams.Add(Team1);
            teams.Add(Team2);

            if (!string.IsNullOrEmpty(Team3))
            {
                teams.Add(Team3);
            }
            if (!string.IsNullOrEmpty(Team4))
            {
                teams.Add(Team4);
            }


            // Navigate to play page with params
            await Shell.Current.GoToAsync(nameof(PlayPage), new Dictionary<string, object>
                {
                    { "PlayParams", new PlayParameter(teams, nbCards,duration,nbPass,nbPenalty,IsHardMode,true)}
                });

        }
    }

}

