﻿using System;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Collections;
using TP01_Taboo_MAUI.Models;
using TP01_Taboo_MAUI.Views;
using CommunityToolkit.Mvvm.Input;

namespace TP01_Taboo_MAUI.ViewModel
{
    public partial class EndViewModel : ObservableObject
    {

        private TabooManager tabooManager;

        [ObservableProperty]
        int scoreTeam1 = 0;

        [ObservableProperty]
        int scoreTeam2 = 0;

        [ObservableProperty]
        int scoreTeam3 = 0;

        [ObservableProperty]
        int scoreTeam4 = 0;

        [ObservableProperty]
        string team1 = "";

        [ObservableProperty]
        string team2 = "";

        [ObservableProperty]
        string team3 = "";

        [ObservableProperty]
        string team4 = "";

        private List<Team> _teams;



        public EndViewModel()
        {
            // Logic of the End View with the medal and scores 
            tabooManager = TabooManager.GetInstance(null);
            _teams = tabooManager.GetTeams();
            UpdateUI();
        }

        void UpdateUI()
        {
            Team1 = _teams[0].name;
            Team2 = _teams[1].name;
            ScoreTeam1 = _teams[0].score;
            ScoreTeam2 = _teams[1].score;

            if (_teams.Count < 3)
            {
                Team3 = "No third team";
                Team4 = "No fourth team";
                return;
            }
            else if (_teams.Count > 3)
            {
                Team3 = _teams[2].name;
                Team4 = _teams[3].name;
                ScoreTeam3 = _teams[2].score;
                ScoreTeam4 = _teams[3].score;
            }
            else
            {
                Team3 = _teams[2].name;
                ScoreTeam3 = _teams[2].score;
                Team4 = "No fourth team";
            }
        }

        // button "Finish"

        [RelayCommand]
        public async void Finish()
        {
            Console.WriteLine("Finish ----------------------");
            tabooManager.ResetGame();
            await Shell.Current.GoToAsync(nameof(HomePage));
        }


    }
}

