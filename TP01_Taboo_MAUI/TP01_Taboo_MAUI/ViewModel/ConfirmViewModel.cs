﻿using System;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using TP01_Taboo_MAUI.Models;
using System.Collections.ObjectModel;
using TP01_Taboo_MAUI.Views;

namespace TP01_Taboo_MAUI.ViewModel
{
    [QueryProperty(nameof(PlayParams), "PlayParams")]
    public partial class ConfirmViewModel : ObservableObject

    {
        private readonly TabooManager tabooManager;

        [ObservableProperty]
        private PlayParameter playParams;

        [ObservableProperty]
        ObservableCollection<Taboo> playedTaboos;

        public ConfirmViewModel()
        {
            tabooManager = TabooManager.GetInstance(null);

            playedTaboos = tabooManager.GetPlayedThisRound();

            for (int i = 0; i < playedTaboos.Count; i++)
            {
                if (playedTaboos[i].State == CardState.WON)
                {
                    playedTaboos[i].Image = "correct.png";
                }
                else if (playedTaboos[i].State == CardState.FAILED)
                {
                    playedTaboos[i].Image = "wrong.png";
                }
                else if (playedTaboos[i].State == CardState.PASSED)
                {
                    playedTaboos[i].Image = "passed.png";
                }
            }

        }


        //*********************************
        // Commands:

        //*********************************
        // Change the state of the button
        // !!!! for instance ChangeState command become ChangeStateCommand by Mvvm toolkit !!!!
        // to put in xaml {Binding ChangeStateCommand}
        [RelayCommand]
        public void ChangeState(string Name)
        {
            // Find the taboo in the list
            for (int i = 0; i < PlayedTaboos.Count; i++)
            {
                if (PlayedTaboos[i].Word == Name)
                {
                    if (PlayedTaboos[i].State == CardState.WON)
                    {
                        PlayedTaboos[i].State = CardState.FAILED;
                        PlayedTaboos[i].Image = "wrong.png";
                    }
                    else if (PlayedTaboos[i].State == CardState.FAILED)
                    {
                        PlayedTaboos[i].State = CardState.PASSED;
                        PlayedTaboos[i].Image = "passed.png";
                    }
                    else if (PlayedTaboos[i].State == CardState.PASSED)
                    {
                        PlayedTaboos[i].State = CardState.WON;
                        PlayedTaboos[i].Image = "correct.png";
                    }
                }
            }
        }


        //*******************
        // Validate command
        // ...

        [RelayCommand]
       async public void Validate()
        {
            tabooManager.ValidateTurn();

            // Check if the game is finished
            if (tabooManager.IsGameOver())
            {
                
                await Shell.Current.GoToAsync(nameof(EndPage));
            }
            else
            {
                PlayParams.NewGame = false;
                await Shell.Current.GoToAsync(nameof(PlayPage), new Dictionary<string, object>
                {
                    { "PlayParams", PlayParams }
                });

            }
        }
    }
}

