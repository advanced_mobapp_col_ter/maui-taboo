﻿using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI.Views;

public partial class EndPage : ContentPage
{

    public EndPage(EndViewModel vm)
	{
        
        InitializeComponent();
        BindingContext = vm;
		
	}

}
