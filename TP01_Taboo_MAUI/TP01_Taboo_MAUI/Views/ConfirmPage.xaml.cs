﻿using TP01_Taboo_MAUI.ViewModel;

namespace TP01_Taboo_MAUI.Views;

public partial class ConfirmPage : ContentPage
{

	public ConfirmPage(ConfirmViewModel vm)
	{
		InitializeComponent();
        BindingContext = vm;
        
    }

}
