﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TP01_Taboo_MAUI.Models;
using TP01_Taboo_MAUI.Views;
using TP01_Taboo_MAUI.ViewModel;


namespace TP01_Taboo_MAUI;

public partial class MainPage : ContentPage
{ 
    public MainPage()
	{
		InitializeComponent();
        this.BindingContext = this;
	}

    async  void OnCliqued(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(HomePage));
    }

}


