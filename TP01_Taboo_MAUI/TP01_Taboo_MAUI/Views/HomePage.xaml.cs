﻿
namespace TP01_Taboo_MAUI.Views;
using TP01_Taboo_MAUI.ViewModel;
public partial class HomePage : ContentPage
{

    // Used for resteting the homePage value!!!
    // To be seen  
    // private string _previousTeam1Value;

    public HomePage(HomeViewModel vm)
    {
        InitializeComponent();
        BindingContext = vm;

    }

}
